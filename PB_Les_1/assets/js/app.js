const firstname = "Ricardo",
lastname = "Rietjens",
age = 17,
locatie = "Middelburg";

const price = 0.59,
number = 200,
totalprice = price * number;

console.log("Hallo, ik ben " + firstname + " " + lastname + " en ik ben " + age + " jaar oud.");
console.log("Ik zit op school in " + locatie);
console.log("De totale prijs van de " + number + " appels is €" + totalprice + ",-");

let message = "Please visit the HZ University of applied science";
console.log(message.replace('applied', 'mad'));
