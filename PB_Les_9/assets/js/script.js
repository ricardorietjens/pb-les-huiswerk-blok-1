// Alle li items selecteren uit de html
const allListItems = document.getElementsByTagName('li');
// Alle list items printen
console.log(allListItems);

// Id selecteren
const ul = document.getElementById('groceryList');
// Eerste artikel van lijstje ophalen
const firstChild = ul.firstElementChild;
// Printen eerste list item
console.log(firstChild);
// Toepassen stijl 'standout' uit css
firstChild.classList.add('standOut');

// List item toevoegen:
// Plek voor nieuw item kiezen
const ph = document.getElementById('groceryList');
// Item maken
const newListItem = document.createElement('li');
// Naam nieuwe list item
newListItem.innerText = 'krop sla';
// Element invoege
ph.appendChild(newListItem);

// Prijs toevoegen aan de producten
const groceryList = [
    {
        'name': 'tandenborstel',
        'price': 0.99
    },
    {
        'name': 'deodorant',
        'price': 2.50
    },
    {
        'name': 'bakmeel',
        'price': 0.79
    },
    {
        'name': 'wortels',
        'price': 1.79
    },
    {
        'name': 'krop sla',
        'price': 0.95
    }
]

// Functie schrijven zodat de data in de tabel in de html komt
function createMyAwesomeTable() {
    // For loop om elk product in een aparte rij in een tabel te plaatsen
    for (r = 0; r < groceryList.length; r++) {
        // Tabel aanmaken
        const table = document.createElement('table');
        // tr aanmaken
        const tr = document.createElement('tr');
        // td aanmaken voor artikelnaam
        const tdName = document.createElement('td');
        // Artikelnaam plaatsen
        tdName.innerText = groceryList[r].name;
        // td aanmaken voor artikelprijs
        const tdPrice = document.createElement('td');
        // Artikelprijs plaatsen
        tdPrice.innerText = groceryList[r].price;
        // Nieuwe regel aanmaken voor artikelnaam
        tr.appendChild(tdName);
        // Nieuwe regel maken voor artikelprijs
        tr.appendChild(tdPrice);
        // Nieuwe regel rij maken in de tabel
        table.appendChild(tr);
        // Body van de html linken met js om tabel te plaatsen
        const body = document.getElementById('body');
        // Nieuwe tabel aanmaken in de body
        body.appendChild(table);
    }
}

// Functie oproepen
createMyAwesomeTable();
