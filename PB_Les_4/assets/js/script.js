// Opdracht 1: Beoordeling de resultaten voor het vak
// Programming basics met een if-else statement
let grade = 8;
document.write("De beoordeling voor het vak programming basics is ");
if (grade < 6) {
    document.write("Onvoldoende<br>");
} else if (grade < 7) {
    document.write("Voldoende<br>");
} else if (grade < 9) {
    document.write("Goed<br>");
} else if (grade === 10){
    document.write("Uitmuntend<br>");
} else {
    document.write("Error<br>")
}


// Opdracht 2: Beoordeling de resultaten voor het vak
// Programming basics met een switch statement
let text;
document.write("De beoordeling voor het vak programming basics is ");
switch (grade) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
        text = 'Onvoldoende<br>';
        break;
    case 7:
        text = 'Voldoende<br>';
        break;
    case 8:
    case 9:
        text = 'Goed<br>';
        break;
    case 10:
        text = 'Uitmuntend<br>';
        break;
    default:
        text = 'Error<br>';
        break;
}
document.write(text);

// Opdracht 3: Codesnippet if-else statement met 3 factoren
let purchasedBook = true;
let job = 'teacher';
let inTrain = true;

if (purchasedBook === true && inTrain === true && job === 'teacher') {
    document.write('Finally i can enjoy my book!');
}

else if (purchasedBook === false && inTrain === true && job === 'teacher') {
    document.write('There isn\'t a book for the teacher in the train!');
}

else if (purchasedBook === false && inTrain === false && job === 'teacher') {
    document.write('There isn\'t a book and a train for the teacher in the train!');
}

else if (purchasedBook === true && inTrain === false && job === 'teacher') {
    document.write('There isn\'t a train for the teacher to read the book!');
}

else if (purchasedBook === false && inTrain === false && job !=='teacher') {
    document.write('There isn\'t a teacher for the book in the train!');
}

else if (purchasedBook === true&& inTrain === true && job !=='teacher') {
    document.write('There isn\'t a teacher in the train!');
}

else if (purchasedBook === false && inTrain === true && job !=='teacher') {
    document.write('There isn\'t a book or a teacher in the train!');
}

else if (purchasedBook === true && inTrain === false && job !=='teacher') {
    document.write('There is only a book!');
}
