window.addEventListener('load', init);

function init() {
    console.log('DOM is loaded');
    showQuestion();
}

let questionText = document.getElementById('questionText');
const submitButton = document.getElementById('submit');
const container = document.getElementById('container');
let answerText = document.getElementById('answerText');

answerText.innerHTML = '';

const binaryAnswers = [{
    binary: '01101000 01100101 01101100 01101100 01101111',
    text: 'Hello'
},
{
    binary: '01010111 01100101 01101100 01101011 01101111 01101101',
    text: 'Welkom'
},
{
    binary: '01010100 01101111 01110100 00100000 01111010 01101001 01100101',
    text: 'Tot ziens'
},
{
    binary: '01000111 01110010 01101111 01100101 01110100 01101010 01100101 01110011',
    text: 'Groetjes'
}
];

function randomizeArray(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
}

let randomBinary = randomizeArray(binaryAnswers);

let randomQuestion = randomBinary[Math.floor(Math.random() * randomBinary.length)];

function showQuestion() {
    let randomBinaryCode = randomQuestion.binary;

    console.log(randomBinaryCode);

    questionText.innerText = randomBinaryCode;
}

submitButton.addEventListener('click', clickEventHandler);

function clickEventHandler(event) {
    event.preventDefault();
    console.log('Submitbutton ingedrukt');
    let inputText = document.getElementById('inputAnswer').value;
    console.log(inputText);

    let randomQuestionAnswer = randomQuestion.text;
    console.log(randomQuestionAnswer);

    if(randomQuestion.text == inputText) {
        console.log('Antwoord is goed!');
        container.classList.remove('bg-red');
        let goodString = `Je antwoord <span class='bg-green'>${inputText}</span> is helemaal goed`;
        answerText.innerHTML = goodString;

        setTimeout(function () { location.reload(true); }, 1500);

    } else {
        
        container.classList.add('bg-red');
        let wrongString = `Helaas, <span class='bg-red'>${inputText}</span> is niet juist, probeer het nog eens`;
        answerText.innerHTML = wrongString;
    }
}