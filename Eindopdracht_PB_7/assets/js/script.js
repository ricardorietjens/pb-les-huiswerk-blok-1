// Lijst met rondetijden
const lapRounds = [
    2.99,
    3.00,
    3.01,
    4.01,
    2.79,
    2.88,
    3.10,
    4.12
];

// Functie om een random rondetijd te kiezen
const getRandomLapRound = function (anArray) {
    return lapRounds[Math.floor(Math.random() * lapRounds.length)];
}

// Aan roepen functie random rondetijd
console.log(getRandomLapRound(lapRounds));

// Opdracht 2: 
const allMyRecords = [
    [
        "The Who - Pinball Wizard",
        "Rolling Stones - Exile on main street",
        "Police - Message in a bottle"
    ],

    [
        "De Dijk - Alle 40 Goed",
        "Het Goede Doel - Belgie",
        "Doe Maar - skunk"
    ]
];

for (let i = 0; i < allMyRecords.length; i++) {
    for (let j = 0; j < allMyRecords[i].length; j++) {
        console.log(allMyRecords[i][j])
    }
}

// Opdracht 3: vanuit document 
// const filteredLapRoundsWithForLoop = function () {
//     let newArray = [];
//     for (let i = 0; i < lapRounds.length; i++) {
//         if (lapRounds[i] < 4) {
//             newArray.push(lapRounds[i]);
//         }
//     }
//     console.log(newArray);
// }
// filteredLapRoundsWithForLoop();

// Opdracht 3: filter
// Functie die alleen getallen kleiner dan 4 print
const everythingUnderFour = lapRounds.filter(function (lapRounds) {
    return lapRounds < 4;
})
// Functie printen
console.log(everythingUnderFour);
