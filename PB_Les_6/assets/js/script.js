// Opdracht names
function giveMeSomeNiceName(gender) {
    let myNewName = "";
    if (gender == "male") {
        myNewName = "Arnold"
    } else if (gender == "female") {
        myNewName = "Rochella"
    } else {
        myNewName = "Error"
    }
    return myNewName;
}

const myName = giveMeSomeNiceName("male");
console.log(`My new name is: ${myName}`);

// Opdracht pigs
var pigs = function (myNumberOfPigs) {
    var pigs = "";
    var counter = 1;
    while (counter <= myNumberOfPigs) {
        pigs += "🐷"
        counter++;
    }
    pigs += " knor!";
    return pigs;
}
console.log(pigs(4));

// Opdracht facorial
function factorialize(num) {
    var result = num;
    if (num == 0 || num === 1) {
        return (result);
    } else {
        while (num > 1) {
            num--;
            result = result * num;
        } return (result);
    }
}
console.log(factorialize(5));

// Opdracht audio toevoegen aan een pagina m.b.v. prompt
let audioDog = new Audio('./assets/animalsounds/dog.mp3');
let audioCat = new Audio('./assets/animalsounds/cat.mp3');
let audioCow = new Audio('./assets/animalsounds/cow.mp3');
let audioHorse = new Audio('./assets/animalsounds/horse.mp3');

function animalSounds(animal) {
    if (animal == "dog") {
        audioDog.play();
    } else if (animal == "cat") {
        audioCat.play();
    } else if (animal == "cow") {
        audioCow.play();
    } else if (animal == "horse") {
        audioHorse.play()
    }
}

function processUserInput(callback) {
    let animal = prompt("Please enter an animal");
    callback(animal);
}

processUserInput(animalSounds);