// Boodschappenlijstje met producten
const productsList = [];
productsList[0] = 'Tandenborstel';
productsList[1] = 'Deodorant';
productsList[2] = 'Bakmeel';
productsList[3] = 'Wortels';
productsList[4] = 'Tandpasta';
productsList[5] = 'Krop sla';
productsList[6] = 'Maggi';
productsList[7] = 'Croky chips';
productsList[8] = 'WC papier';
productsList[9] = 'Shampoo';

// Een product toevoegen
productsList.push('Cola');

// Laatste product verwijderen
// productsList.pop();

// Aanroepen tabel boodschappenlijst
console.table(productsList);

// Alle producten opsommen met de tekst 'element ...'
productsList.forEach(function (element, index, myArray) {
    console.log('element', element);
});

// Opsomming van alle producten met tekst
// 'Op index: ... staat element: ...
for (let index = 0; index < productsList.length; index++) {
    const element = productsList[index];
    console.log(`Op index: ${index} staat element: ${element}`);
}

// Zorgen dat de elementen dikgedrukt zijn 
let newProductsListCapital = productsList.map(function (element) {
    let newElement = element.toUpperCase();
    return newElement;
})

// Aanroepen dikgedrukte tabel boodschappenlijst
console.table(newProductsListCapital);

// Element definïeren
let newProductsListArray = [];

// Genummerde opsomming van producten met tekst 'Product ... : ...'
for (let index = 0; index < productsList.length; index++) {
    const element = `Product ${index + 1}: ${productsList[index]}`;
    newProductsListArray.push(element);
}

// Aanroepen tabel boodschappenlijst met nummmers met for loop
console.table(newProductsListArray);

// Genummerde opsomming van producten met tekst 'Product ... : ...'
let newProductsList = productsList.map(function (element, index) {
    let newElement = `Product ${index + 1}: ${productsList[index]}`;
    return newElement;
})

// Aanroepen tabel booschappenlijst met nummers met functie map (beter)
console.table(newProductsList);

// Alle prijzen optellen
const productsPrices = [
    2.10,
    4.99,
    5.60,
    0.40,
    5.44,
    7.33,
    2.33,
    2.49,
    2.10
];

// Functie voor optellen prijzen
let totalSum = 0;
for (let index = 0; index < productsPrices.length; index++) {
    totalSum = totalSum + productsPrices[index];
}

// Aanroepen functie totaalprijs
console.log(totalSum);

// Gemiddelde prijs berekenen
const average = function (anArray) {
    // Totaalprijs delen door het aantal getallen
    let newAverage = totalSum / anArray.length;
    // Afronden op 2 decimalen
    newAverage = Math.round(newAverage * 100) / 100;
    return newAverage;
}

// Uitkomst gemiddelde prijs
console.log(average(productsPrices));

