// Number is odd or even 
let num = 3;
document.write("Number = " + num + "<br>");
if (num % 5 == 0) {
    document.write('Number is odd! <br><br>');
} else {
    document.write('Number is even! <br><br>');
}

// Cut the word 'not'
const message = "Programming is not so cool. <br><br>";
document.write(message.replace('not ', ''));

// Compare '1400' and 'Ik woon in Naboo’
const a = 1400,
b = 'Ik woon in Naboo.';
if (a == b) {
    document.write("De twee waardes zijn gelijk.");
} else {
    document.write("De twee waardes zijn niet gelijk.");
}
// Dit is geen slimme vraag omdat je een getal met een zin vergelijkt,
// die kunnen helemaal niet gelijk zijn aan elkaar.
// Een getal is heel wat anders dan een zin (string), dus deze functie heeft geen nut.