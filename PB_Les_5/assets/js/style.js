// While
// Opdracht 1
myNumber= 0;

while (myNumber < 10) {
    myNumber++;
    console.log(myNumber);
}
 
// Oprdacht 2
const message = "Het is maandag!";
var y = message.length;
console.log(message.length);

while(y>=0){
  y--;
  console.log(message.charAt(y));
}

// Opdracht 3
const word = "Geweldig";
let reversedWord = "";
let count = word.length; //8

while (count > 0 ) {
    reversedWord = reversedWord + word.charAt(count-1);
    console.log(reversedWord);
    count--;
}

console.log(reversedWord);

// For
// Opdracht 1
for (let myNumber = 0; myNumber <= 10; myNumber++) {
    console.log(myNumber);
}

// Opdracht 2
const sentence = "Het is maandag!";
for (let i = 0; i < sentence.length; i++) {
    console.log(sentence.charAt(i));
}

// Opdracht 3
for (let i = 1; i <= 25; i++) {
  if(i % 3 == 0){
      console.log(`${i} is deelbaar door 3`);
  } else { 
      console.log(`${i} niet deelbaar door 3`);
    }
}