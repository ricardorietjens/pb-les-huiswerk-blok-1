// Opdracht 1: Bereken het gemiddelde op basis van de cijfers die in de tabel staan 

function addRow() {
    const list = document.getElementsByTagName('tr');

    let gradesArray = [];
    let gradesArraySum = 0;

    for (let index = 0; index < list.length; index++) {
        let listItem = list[index];
        let getGrade = listItem.lastElementChild.innerText;
        let parsedValue = parseFloat(getGrade);
        let generateArray = gradesArray.push(parsedValue);
        let averageGrade = gradesArraySum += gradesArray[index];
    }


    // Alle cijfers opgeteld delen door het aantal cijfers
    let overallGrade = gradesArraySum / list.length;

    // Tabel uit html ophalen
    const table = document.getElementById('table');
    // Nieuw tr aanmaken door aantal bestaande rijen te tellen
    let row = table.insertRow(list.length);

    // Nieuwe cel invoegen in kolom 0
    var cell1 = row.insertCell(0);
    // Nieuwe cel invoegen in kolom 1
    var cell2 = row.insertCell(1);

    // Tekst toevoegen aan nieuwe cel in kolom 0
    cell1.innerText = "Gemiddelde cijfer";
    // Waarde toevoegen aan nieuwe cell in kolom 1
    cell2.innerText = overallGrade.toFixed(1);
}

addRow();

// Ordracht 2: alle even elementen krijgen een andere achtergrondkleur
function colorChange() {
    // Selecteer alle li elementen
    let lists = document.getElementsByTagName('li');
    // Maak een for loop waarbij list gezien wordt als getal
    for (let list in lists) {
        // Wanneer het getal deelbaar is door 2 is het even
        // Link de even getallen aan de css style
        if (list % 2 == 0) {
            lists[list].classList.add('colorChange');
        }
    }
}

colorChange();


// Opdracht 3: functie voor een plaatje in JS
function createImageElement(imageSrcName) {
    let imagePlace = document.getElementById('imageDiv');
    var createImage = document.createElement("IMG");
    imageSrcName = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTomNJ5OTTTJ-EPevns1xORg9h2L6UxEPOH_b4gb5jCyy_VvG_n9A';
    createImage.src = imageSrcName;
    imagePlace.appendChild(createImage);
}

createImageElement();