// Opdracht 1: Geef de rondetijden weer
lapRounds = {
    0: 55.99,
    1: 63.00, 
    2: 63.01, 
    3: 54.01, 
    4: 62.79, 
    5: 52.88, 
    6: 53.10, 
    7: 54.12
};

// Print rondetijden
console.log(lapRounds);

// Opdracht 2: Gegevens per docent
const teachers = [
    // Gegevens Loek
    {
        name: "Loek",
        profession: "Teacher",
        brand: "Linux",
        hoursPerWeek: 40,
        salary: 2400,
        salaryPerHour: function () {
            return this.salary / this.hoursPerWeek
        }
    },

    // Gegevens Daan
    {
        name: 'Daan',
        profession: 'Teacher',
        brand: 'Arduino',
        hoursPerWeek: 40,
        salary: 3500,
        salaryPerHour: function () {
            return this.salary / this.hoursPerWeek
        }
    },
    // Gegevens Rimmert
    {
        name: 'Rimmert',
        profession: 'Teacher',
        brand: 'Apple',
        hoursPerWeek: 40,
        salary: 2800,
        salaryPerHour: function () {
            return this.salary / this.hoursPerWeek
        }
    }
]

// Opdracht 3: Schrijf een functie om de gegevend per docent weer te geven
for (index = 0; index < teachers.length; index++) {
    // Zet de benodigde informatie in variabelen
    teacherName = teachers[index].name;
    teacherProfession = teachers[index].profession;
    teacherBrand = teachers[index].brand;
    teacherSalary = teachers[index].salaryPerHour();

    // Print de zinnen met de gegevens per docent
    console.log(`I have a ${teacherProfession} named ${teacherName} and he likes to work on a ${teacherBrand} computer. He earns €${teacherSalary} per hour.`);
}
