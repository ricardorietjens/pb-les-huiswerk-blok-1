// Opdracht 1: Clicker Game
// Counter definiëren en deze als begin op 0 zetten als beginwaarde.
let counter = 0;

// Controleren of counter 0 is, zo ja; nieuwe H1 aanmaken en in DOM zetten
if (counter == 0) {
    // Plaats voor H1-Element
    const placeOfNewClicker = document.getElementById('clickerContainer');
    // H1-Element maken
    const newClicker = document.createElement('h1');
    // H1-Element het ID 'showCounter' geven
    newClicker.setAttribute('id', 'showCounter');
    // Nieuwe element de inhoud van de counter als tekst meegeven
    newClicker.innerText = counter;
    // Nieuwe element pushen naar DOM in de DIV 'ClickerContainer'
    placeOfNewClicker.appendChild(newClicker);
}

// Button ophalen uit DOM en deze in de variabele 'getClicker' zetten
const getClicker = document.getElementById('clickerButton');

// Functie die de waarde van counter controleert en het ID bij bepaalde waarde aanpast
function checkCounter(counter) {
    // 10-20
    if (counter == 10) {
        getClicker.id = 'clickerButtonSmaller1';
        // 20-3o0
    } else if (counter == 20) {
        getClicker.id = 'clickerButtonSmaller2';
        // 30-40
    } else if (counter == 30) {
        getClicker.id = 'clickerButtonSmaller3';
    }
}

// Bij indrukken button, voer functie clickEventHandler uit
getClicker.addEventListener('click', clickEventHandler);

// Functie die uitgevoerd moet bij indrukken button
function clickEventHandler(event) {
    // Counter ophalen
    const getCounter = document.getElementById('showCounter');
    // Per druk op de knop 1 optellen bij de waarde van de counter
    counter++;
    // Nieuwe waarde toevoegen
    getCounter.innerText = counter;
    // Functie checkCounter uitvoeren om te controleren of de waarde 10 of 20 is
    checkCounter(counter);
}


// OPDRACHT 2: Maak een boodschappenlijstje
// Add-button ophalen uit DOM en deze in de variabele 'getAddButton' zetten
const getAddButton = document.getElementById('submitButton');

// Bij indrukken button, voer functie clickEventHandler uit
getAddButton.addEventListener('click', groceryClickEventHandler);

// Variabele voor totaalprijs definiëren en beginwaarde 0 geven
let totalPrice = 0;

// Wanneer er een nieuw item wordt toegevoegd, voer deze functie uit
function groceryClickEventHandler(event) {
    // PreventDefault zorgt ervoor dat de pagina niet refresht
    event.preventDefault();
    // Haal inputwaarde van productnaam op
    const inputText = document.getElementById('textInput').value;
    console.log(inputText);
    // Haal inputwaarde van productprijs op
    const priceText = document.getElementById('priceInput').value;
    console.log(priceText);
    // Wanneer inputtext leeg is, geef een waarschuwing weer
    if (inputText == '') {
        alert('Voer alle gegevens in!');
    } else {
        // Haal de tabel op
        const placeOfNewEntry = document.getElementById('groceryTable');
        // Maak een nieuwe rij aan
        const newListItem = document.createElement('tr');
        // Maak een nieuwe kolom aan
        const productName = document.createElement('td');
        // Zet de productnaam uit de input die eerder wordt gedefinieerd in de kolom
        productName.innerText = inputText;
        // Maak een nieuwe kolom aan
        const productPrice = document.createElement('td');
        // Zet de prijs uit de input die eerder wordt gedefinieerd in de kolom
        productPrice.innerText = `€${priceText}`;
        // Maak een nieuwe button aan
        let deleteProduct = document.createElement('td');
        // Maak van de delete td een delete link
        deleteProduct.innerHTML = '<a href= "#" >verwijder<a/>'

        // Zorg ervoor dat de link werkt als je erop klikt
        deleteProduct.addEventListener('click', deleteRow);

        //Fuctie die ervoor zorgt dat het product verwijderd wordt bij klikken op 'verwijderen'
        function deleteRow(event) {
            // Selecteer de goede rij in de tabel
            let tr = event.target.parentNode.parentNode
            // Verwijder de geselecteerde rij
            groceryTable.removeChild(tr)
        }

        // Voeg alle childs toe aan het document
        placeOfNewEntry.appendChild(newListItem);
        newListItem.appendChild(productName);
        newListItem.appendChild(productPrice);
        newListItem.appendChild(deleteProduct);

        // Haal het totaalprijs-element op uit HTML
        const tableTotalPrice = document.getElementById('totalPrice');
        // Maak van de prijs een float zodat ermee gerekend kan worden
        priceTextToFloat = parseFloat(priceText);
        // Bereken de nieuwe totaalprijs
        newTotalPrice = totalPrice + priceTextToFloat;
        // Zet de nieuwe totaalprijs in de totalPrice-variabele
        totalPrice = newTotalPrice;
        // Zet de totaalprijs in het document
        tableTotalPrice.innerText = `€${totalPrice}`;

    }
}