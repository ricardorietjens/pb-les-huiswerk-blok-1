// Opdracht 1: functie maken van de Fibonacci getallenreeks 
function getSum() {
    let number = [2, 4, 8, 9, 12, 13];
    let sum = 0;
    for (let i = 0; i < number.length; i++) {
        sum += number[i]
    }
    console.log(`Your sum is ${sum}`);
}
getSum();


// Opdracht 2: een functie countdown die van 10 - 1 aftelt
// en bij 0 de melding "Happy + het huisdige jaartal" weergeeft.
function countDown(seconds) {
// Zorgt ervoor dat de huidige seconden weergegeven worden
    if (seconds) {
        console.log(seconds + " seconden te gaan...");
        setTimeout(countDown, 1000, --seconds);
// Zorgt ervoor dat het huidige jaartal weergegeven wordt
    } else {
        var d = new Date;
        var currentYear = d.getFullYear();
        console.log(`Happy ${currentYear}!`);
    }
}
// Waarde van 10 seconden
countDown(10);


// Opdracht 3: functie expressie en declaratie waarmee
// je aantoont dat een expressie en een declaratie op
// een andere manier met hoisting omgaan.
// Hoisting: een variabele al gebruiken voor je deze declareert

function declare() {
    //Waardes aangeven voordat deze zijn gedeclareerd
    a = 10;
    b = 20;

    // Waarde in de console zonder te rekenen, declaratie
    console.log(`Het getal is ${a} en ${b}`);

    // Declareer ze nu pas
    var a;
    var b;
}

// Functie werkt, ookal worden de variabelen
// later gedeclareerd dan ze worden gebruikt
declare();

// Expressie, samenvoegen van meerdere variabelen in een rekensom
function expression() {
    var y = 20,
        stringOne = "Het is vandaag ";

    // Gebruik alle variabelen in expressions
    var calc = z + y;
    console.log(calc);
    console.log(stringOne + stringTwo);

    var z = 10,
        stringTwo = "dinsdag";
}
// Werkt niet (som geeft NaN, string geeft undefined)
// In expressions moeten alle variabelen gedeclareerd zijn voor de worden gebruikt
expression();