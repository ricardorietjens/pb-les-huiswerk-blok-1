// Een object met objecten maken in js
let ricardo = {
    name: 'Ricardo',
    age: 17,
    previousdiploma: 'HAVO',
    // Object in object
    favouriteVihicle: {
        name: 'scooter',
        brand: 'Peugeot',
        model: 'Speedfight 3',
        year: '2011',
        numberOfWheels: 2,
        fuel: 'Benzine'
    },
    // Array
    familyNames: [
        'Alouschka',
        'Ineke',
        'Boudewijn',
        'Rochella',
        'Tessa'
    ],
    // Namen onder elkaar nummeren
    printFamilyNames: function () {
        for (let i = 0; i < ricardo.familyNames.length; i++) {
            console.log(`Familielid ${i} is ${ricardo.familyNames[i]}`)
        }
    }
};

// let ricardo printen als object
console.log(ricardo);

// dot-notatie
ricardo.nameStudy = 'HBO-ICT';
ricardo.startYearStudy = 2018;
ricardo.locationStudy = 'Middelburg';

// Namen onder elkaar printen
ricardo.printFamilyNames();

// Gegevens printen in een zin
console.log(`Ik ben ${ricardo.name} en ik ben ${ricardo.age} jaar oud.`)
// Specifieke zin printen
console.log(`Mijn favoriete vervoersmiddel is mijn ${ricardo.favouriteVihicle.name}. Hij heeft
${ricardo.favouriteVihicle.numberOfWheels} wielen en rijdt op ${ricardo.favouriteVihicle.fuel}.`);
