// Opdracht 1: while loop die controleert
// welke getallen tuseen 0 en 40 deelbaar zijn door 4
let i = 0;

while (i < 40) {
    i++;
    if (i % 4 == 0) {
        console.log(`${i} Is deelbaar door 4`);
    } else { 
        console.log(`${i} Niet deelbaar door 4`);   
  }
}

// Opdracht 2: Een Fibonacci getallenreeks

var x = 0;
var y = 1;
while(y < 56) {
    var tmp = x + y;
    x = y;   // = tmp - x
    y = tmp;
    console.log(y);
}

// Opdracht 3: Een getallenreeks optellen
let number = [2, 4, 8, 9, 12, 13];

let sum = 0;

for (let i = 0; i < number.length; i++) {
    sum += number[i]
}
console.log(`Your sum is ${sum}`);