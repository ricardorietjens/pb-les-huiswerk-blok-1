// comment via JSDocs: http://usejsdoc.org/about-getting-started.html
/**
 * Vraag 1
 * Gegeven is de volgende array van employees. Maak een functie die de oorspronkelijke array
 * als argument neemt en een nieuwe array retourneert waarbij de het salaris met 10% is verhoogt.
 * 
 */
const employees = [{
    name: 'Jan',
    surname: 'Jansen',
    salary: 35000
},
{
    name: 'Kees',
    surname: 'Groot',
    salary: 27000
},
{
    name: 'Bert',
    surname: 'Pietersen',
    salary: 30000
},
{
    name: 'Wimpie',
    surname: 'Jobsen',
    salary: 21000
}
];

function increaseSalary(percentage) {
    let newSalary = [];
    for (let i = 0; i < employees.length; i++) {
        let newObject = {
            name: employees[i].name,
            surname: employees[i].surname,
            salary: Math.round(employees[i].salary * percentage)
        }
        newSalary.push(newObject)
    }
    return newSalary;
};

/**
 * Vraag 2
 * Gegeven zijn de volgende categorien voertuigen.
 * Categorie 1: auto's, busjes, campers of bestelwagens, waarbij het hoogste punt van het voertuig maximaal 2 meter bedraagt.
 * Categorie 2: auto's, busjes, campers of bestelwagens, waarbij het hoogste punt van het voertuig meer dan 2 meter, maar maximaal 3 meter bedraagt.
 * Categorie 3: campers, bussen en bedrijfsvoertuigen met een hoogte van meer dan 3 meter of een toegestane maximummassa van meer dan 3500 kg.
 * Maak een beslisstructuur die precies aangeeft in welke categorie een voertuig valt. Maar daarbij gebruik van de onderstaande voertuig object.  
 */

const myVans = [
    {
        type: 'auto',
        weight: 1300,
        height: 2.05
    },
    {
        type: 'scooter',
        weight: 100,
        height: 1.05
    },
    {
        type: 'bus',
        weight: 3000,
        height: 2.70
    },
    {
        type: 'truck',
        weight: 4000,
        height: 2.50
    }
]

function placeInClass() {
    for (let i = 0; i < myVans.length; i++) {
        // Cat 1
        if (myVans[i].height <= 2.00) {
            console.log('Het voertuig ' + myVans[i].type + ' valt in categorie: ' + 1)
            // Cat 2
        } else if (myVans[i].height > 2.00 && myVans[i].height < 3.00 && myVans[i].weight < 3500) {
            console.log('Het voertuig ' + myVans[i].type + ' valt in categorie: ' + 2)
            // Cat 3
        } else if (myVans[i].height > 3.00 || myVans[i].weight > 3500) {
            console.log('Het voertuig ' + myVans[i].type + ' valt in categorie: ' + 3)

        } else {
            console.log('Het voertuig valt niet in de juiste categorie')
        }
    }
};


/**
 * Vraag 3
 * a) in onderstaand stukje code wordt voornamelijk vars gebruikt. Beargumenteer waarom je misschien beter een const of let kan gebruiken. Doe dit in het commentaar.
 * b) in onderstaand stukje code wordt veel code herhaald. Schrijf een generieke functie die de volgende elementen kan maken p, h1 en img
 */

const placeholder = document.getElementById('wrapper');


function makeHeadline() {
    const h1 = document.createElement('h1');
    h1.classList.add('headlines');
    h1.innerText = 'myNewText'
    placeholder.appendChild(h1);
}

function makeParagraph() {
    const p = document.createElement('p');
    p.classList.add('article');
    p.innerText = 'new paragraph';
    placeholder.appendChild(p);
}

function makeImage() {
    const image = document.createElement('img');
    image.src = './assets/img/mario.png';
    image.height = 200;
    placeholder.appendChild(image);
}

// Je kan beter const gebruiken omdat je steeds dezelfde handeling gebruikt

// wait until window is loaded
window.addEventListener("load", init);

/**
 * function to initialize the website
 */
function init() {
    //roep hier waar mogelijk je antwoorden aan.
    console.log('New salary array: ', increaseSalary(1.1))
    placeInClass()
    makeHeadline()
    makeParagraph()
    makeImage()
}