// global variables
const product = {
    prijs: 9.99,
    merk: 'Robijn',
    img: './assets/img/wasmiddel.png'
}

// wait until window is loaded
window.addEventListener("load", init);

/**
 * function to initialize the website
 * @param {event} - event
 */


// Functie voor een plaatje in JS
function createImageElement(imageSrcName) {
    let imgName = product.img
    console.log(imgName)
    let imagePlace = document.getElementById('image')
    var createImage = document.createElement("IMG")
    imageSrcName = imgName
    createImage.src = imageSrcName
    imagePlace.appendChild(createImage)
};

// Submit-button ophalen uit DOM en deze in de variabele 'SubmitButton' zetten
const submitButton = document.getElementById('submit');

// Bij indrukken button, voer functie clickEventHandler uit
submitButton.addEventListener('click', clickEventHandler);

// Bepaal waar de nieuwe tekst moet komen
const placeholder = document.getElementById('message');

// Bepaal welk element er aangemaakt moet worden
const p = document.createElement('p');

// DOM
function createMessage() {
    p.classList.add('article');
    placeholder.appendChild(p);
};

// Weer te geven tekst bij een juist geraden prijs
function winPrize() {
    createMessage()
    p.innerText = 'Je hebt gewonnen!'
};

// Weer te geven tekst bij een onjuist geraden prijs
function losePrize() {
    createMessage()
    p.innerText = 'Helaas je hebt het niet goed geraden'
};

function clickEventHandler(event) {
    // Haal de ingevulde waarde op uit het dom
    const inputText = document.getElementById('price').value;
    console.log(inputText);

    // Oorspronkelijke productprijs
    let imgPrice = product.prijs;
    // Prijs die minimaal ingevoerd moet worden
    let minPrice = imgPrice * 0.90;

    // Wanneer inputtext leeg is, geef een waarschuwing weer
    if (inputText == '') {
        alert('Voer een prijs in!');
    } else if (inputText <= imgPrice && inputText >= minPrice) {
        winPrize()
    } else {
        losePrize()
    }
    // PreventDefault zorgt ervoor dat de pagina niet refresht
    event.preventDefault();
};

function init(event) {
    createImageElement();
}